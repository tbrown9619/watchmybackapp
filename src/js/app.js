/**
 * Welcome to Pebble.js!
 *
 * This is where you write your app.
 */
var UI = require('ui');
var Vector2 = require('vector2');
var Vibe = require('ui/vibe');
var ajax = require('ajax');

// override defaults - no need for InboxReceivedHandler vs. just keeping it in JS
 

var main = new UI.Card({
 title: 'Worried?',
 //subtitle: '\n' + 'Worried?',
 body: '\n' + 'Press select to start timer.' + '\n' + 'Press up for help.',
 subtitleColor: 'indigo', // Named colors
 bodyColor: '#9a0036   ' // Hex colors
});

main.show();

//Click up is where the how to guide resides.
main.on('click', 'up', function(e) {
 var card = new UI.Card({
   body:  'Long press the select button in an emergency. It acts' +
       ' as a panic button, immediately sending a message to your ' +
       'emergency contact(s) with an explanation and location data. ' + '\n' +
       'Press the bottom button to set the length of the timer. This ' +
       'function behaves as an emergency backup system for times you' + 
       ' anticipate your safety may be put in jeopardy.' + '\n' + '\n' + 'Press the select ' +
       'button to start the timer for the emergency backup system. If you ' +
       'arrive safely at your destination, simply press the select button ' +
       'again before the timer runs out. If you fail to do so, your emergency ' +
       'contact(s) will receive a message as described above.'
  });
 card.scrollable(true);
 card.fullscreen(true);
 card.show();
});

main.on('click', 'select', function(e) {
    var wind = new UI.Window({
        backgroundColor: 'black'
    });

    var radial = new UI.Radial({
        size: new Vector2(140, 140),
        angle: 0,
        angle2: 360,
        radius: 20,
        borderColor: 'celeste',
        borderWidth: 1,
    });

    var textfield = new UI.Text({
        size: new Vector2(140, 60),
        font: 'ROBOTO_BOLD_SUBSET_49',
        text: '',
        textAlign: 'center'
    });

    var card = new UI.Card({
        title: '\n' + 'COUNTDOWN CANCELLED'
    });

    var timerDone = new UI.Card({
        title: 'AN ALERT HAS BEEN SENT TO YOUR EMERGENCY CONTACT'
    });
  
    var windSize = wind.size();
    // Center the radial in the window
    var radialPos = radial.position()
        .addSelf(windSize)
        .subSelf(radial.size())
        .multiplyScalar(0.5);
    radial.position(radialPos);
    // Center the textfield in the window
    var textfieldPos = textfield.position()
        .addSelf(windSize)
        .subSelf(textfield.size())
        .multiplyScalar(0.5);
    textfield.position(textfieldPos);
    wind.add(radial);
    wind.add(textfield);
    wind.show();
    var timerRunning = true;
    
    wind.on('click', 'select', function(e) {
        timerRunning = !timerRunning;
        card.show();
        setTimeout(function() {
           // Display the mainScreen
           card.hide();
        }, 2000);
        wind.hide();
    });
    wind.on('longClick', 'select', function(e) {
          timerRunning = false;
          timerEnd();
      });

    //Have a timer and a variable that check to see if the timer should still be counting down
    setInterval(timerLoopCheck, 1000);

    function timerLoopCheck() {
        //Create a timer with an interval equal to about the refresh rate of the pebble's screen
        if (timerRunning) {
            timerCountDown();
        }
    }

    //Define these variables before the timer count down since we need them inside of it
    //The number of seconds we want the timer to be.
    var timerStartMinutes = parseInt(localStorage.getItem('minutesForTimer'));
    var timerStartSeconds = parseInt(localStorage.getItem('secondsForTimer'));
    console.log(timerStartMinutes);
    console.log(timerStartSeconds);
    
    //We want to start the angle at a full value.
    var angleOfRadial = 360;
    var angleOfRadial2 = 360;
    var minutesPassed = 0;
    var secondsPassed = 0;
    // keeping track of how many minutes have passed
  
    function timerCountDown(){
      if(timerStartMinutes-minutesPassed > 1){
        countDownMinutes(6);
      }
      else  if(timerStartMinutes-minutesPassed == 1){
            timerStartSeconds += 60 * (timerStartMinutes - minutesPassed);
            minutesPassed++;
            countDownSeconds(360/timerStartSeconds);
      }
      else{
        countDownSeconds(360/timerStartSeconds);
      }
    }
    function countDownMinutes(angleDecreaseAmt){
      //If we have reached 0 seconds remaining, then we want to stop the timer.
        if (angleOfRadial <= 0) {
          minutesPassed++;
          angleOfRadial = 360;
        } 
        else {
            angleOfRadial -= angleDecreaseAmt;
        }
        
        //We want to decrease angle 2 since we want the timer to appear to be going down.
        textfield.text(timerStartMinutes - minutesPassed);
        radial.angle2(angleOfRadial);
      }
        
    function countDownSeconds(angleDecreaseAmt) {
     
      //If we have reached 0 seconds remaining, then we want to stop the timer.
        if (timerStartSeconds - secondsPassed === 0) {
          timerRunning = false;
          timerEnd();
        } 
        else {
            secondsPassed++;
            angleOfRadial2 -= angleDecreaseAmt;
        }
    
        //We want to decrease angle 2 since we want the timer to appear to be going down.
        radial.angle2(angleOfRadial2);
        //If the angle can be divided by the timer to angle value, that means its a second with no variables
        //This is when we want to update the timer to dispaly the new seconds remaining.
        textfield.text(timerStartSeconds - secondsPassed);
        if (timerStartSeconds - secondsPassed <= 10 && timerStartSeconds - secondsPassed > 5) {
            Vibe.vibrate('long');
        } 
        else if (timerStartSeconds - secondsPassed <= 5) {
            Vibe.vibrate('double');
        }
    }
      
    //Function that handles what we do if the timer reaches 0.
    function timerEnd() {
        //We initialize the message we are going to eventually want to send out to the users contacts. 
        var messageToSendOut;
        //If we are able to successfully obtain the gps postion from the user
        function success(pos) {
            var lat = pos.coords.latitude;
            var lon = pos.coords.longitude;
            var googleMapsURL = 'https://www.google.com/maps/preview/@' + lat + ',' + lon + ',20z';
            messageToSendOut = "We recieved an alarm from a user who has you listed as an emergency contact." +
                " This was their last know location: " + googleMapsURL;
            ajaxCall(messageToSendOut);

        }
      
        
      
        //If we get an error then they must have location services turned off.
        function error(err) {
            messageToSendOut = "We recieved an alarm from a user who has you listed as an emergency contact." +
                "Unfortunately we were not able to determine their location.";
            ajaxCall(messageToSendOut);
        }
        // Choose options about the data returned
        var options = {
            enableHighAccuracy: true,
            maximumAge: 10000,
            timeout: 10000
        };

        function ajaxCall(bodyMessage) {
            var data = {};
            data.To = "4148520131";
            data.From = "4142550845";
            data.Body = bodyMessage;

            ajax({
                    url: 'https://ACd99960d3dfc18229010441bc962cedd3:bf3e76e626b5938e791491cde2972945@api.twilio.com/2010-04-01/Accounts/ACd99960d3dfc18229010441bc962cedd3/Messages.json',
                    method: 'POST',
                    type: 'JSON',
                    data: data
                },
                function(data) {
                  timerDone.show();
                  setTimeout(function() {
                  // Display the mainScreen
                  timerDone.hide();
                  }, 2000);
                  wind.hide();
                  //We could do something with the API repsonse here, or not.
                },
                function(error) {
                    //If we errored here then for some reason the number wasn't able to go through
                    //This could eventually be reduced to make it so that when the user first 
                }
            );
        }

        // Request current position
        navigator.geolocation.getCurrentPosition(success, error, options);

    }

});




main.on('click', 'down', function(e) {
    var settingMinutes = true;
    var seconds = localStorage.getItem('secondsForTimer') || 0;
    var minutes = localStorage.getItem('minutesForTimer') || 0;
    
    // create window
    var wind = new UI.Window({
        backgroundColor: 'black'
    });

    // create text field
    var textfield = new UI.Text({
        size: new Vector2(140, 60),
        font: 'gothic-18-bold',
        text: 'Time of Countdown:',
        textAlign: 'center'
    });

    //start on minutes

    //check which number we're setting (mins or secs) (should start with minutes)
    //whichever one we're setting, make font bigger/bold/different so user knows
    //incement/decrement with user input (buttons)
    //if select is pressed, move on to next state
    //if select is pressed when on final state, exit back to main app screen displaying new timer value

    var textfieldOne = new UI.Text({
        size: new Vector2(140, 60),
        font: 'gothic-28-bold',
        text: minutes + ':' + seconds,
        textAlign: 'center'
    });

    var textfieldUnder = new UI.Text({
        size: new Vector2(140, 60),
        font: 'gothic-18-bold',
        text: '(Setting Minutes)',
        textAlign: 'center'
    });


    var windSize = wind.size();
    var textfieldPos = textfield.position()
        .addSelf(windSize)
        .subSelf(textfield.size())
        .multiplyScalar(0.2);
    textfield.position(textfieldPos);
  
    var textfieldPosOne = textfieldOne.position()
        .addSelf(windSize)
        .subSelf(textfield.size())
        .multiplyScalar(0.8);
    textfieldOne.position(textfieldPosOne);
  
    var textfieldUnderPos = textfieldUnder.position()
        .addSelf(windSize)
        .subSelf(textfield.size())
        .multiplyScalar(1.1);
    textfieldUnder.position(textfieldUnderPos);

    wind.add(textfield);
    wind.add(textfieldOne);
    wind.add(textfieldUnder);

    // display
    wind.show();

    function formatNums() {
        if (seconds < 10 && minutes < 10) {
        textfieldOne.text('0' + minutes + ':' + '0' + seconds);
   }
   else if (seconds < 10) {
     textfieldOne.text(minutes + ':' + '0' + seconds);
   }
   else if (minutes < 10) {
     textfieldOne.text('0' + minutes + ':' + seconds);
   }
   else {
     textfieldOne.text(minutes + ':' + seconds);
   }
    }
  

    formatNums();

    // click up
    wind.on('click', 'up', function(e) {
        if (!settingMinutes) {
            ++seconds;          
        } else {
            ++minutes;
        }
      formatNums();
    });


    // click down
    wind.on('click', 'down', function(e) {
        if (!settingMinutes) {
            --seconds;
        } else {
          --minutes;
        }
      formatNums();

    });

    // click select
    wind.on('click', 'select', function(e) {
        if (settingMinutes) {
            settingMinutes = false;
            textfieldUnder.text('(Setting Seconds)');
        } else {
            // saved graphic
            // go back to previous screen
            localStorage.setItem('secondsForTimer', seconds);
            localStorage.setItem('minutesForTimer', minutes);

            wind.hide();
        }

    });
});